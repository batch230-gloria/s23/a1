let trainer = {
	name: "Ash Kechum",
	age: 10,
	pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
	friends:{
		hoenn:["May", "Max"],
		kanto:["Brock", "Misty"]
	},
	talk: function(){
				console.log(trainer.pokemon[0] + "! I choose you!");
			}

}
console.log(trainer);

console.log("Result of dot notation:")
console.log(trainer.name);

console.log("Result of square bracket notation:")
console.log(trainer["pokemon"]);

console.log("Result of talk method")
trainer.talk();

function Pokemon(name, level, health, ){

		this.name = name;
		this.level = level;
		this.health = health;
		this.attack = level;
		this.tackle = function(target){
			console.log(this.name + " tackled " + target.name);
			target.health -= this.attack;
			console.log(target.name + "'s health is now reduced to "+ target.health);
		}
		this.faint = function(target){
			console.log(this.name + " fainted");
		}
		
	}

	let pikachu = new Pokemon("Pikachu", 12, 24,);
	console.log(pikachu);

	let geodude = new Pokemon("Geodude", 8, 16);
	console.log(geodude);

	let mewtwo = new Pokemon("Mewtwo", 100, 200);
	console.log(mewtwo);

	geodude.tackle(pikachu);
	console.log(pikachu);

	mewtwo.tackle(geodude);
	geodude.faint(geodude)
	console.log(geodude);
